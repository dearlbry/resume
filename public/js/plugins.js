// Avoid `console` errors in browsers that lack a console.
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

// Place any jQuery/helper plugins in here.
(function($)
{
	/*

	Large portions of this tour were takeng from http://tympanus.net/codrops/2010/12/21/website-tour/

	the json config obj.
	name: the class given to the element where you want the tooltip to appear
	bgcolor: the background color of the tooltip
	color: the color of the tooltip text
	text: the text inside the tooltip
	time: if automatic tour, then this is the time in ms for this step
	position: the position of the tip. Possible values are
		TL	top left
		TR  top right
		BL  bottom left
		BR  bottom right
		LT  left top
		LB  left bottom
		RT  right top
		RB  right bottom
		T   top
		R   right
		B   bottom
		L   left
	 */


	var settings = {
		onStart: $.noop,
		onStep:  $.noop,
		onEnd:   $.noop,
		title:   '',
	}

	var tourInProgress = false;
	var step = 0;
	var total_steps;
	var tour = [];

	var methods = {

		init: function(options)
		{
			if (options)
				$.extend(settings, options);

			$(".tooltip").draggable();

			return this.each(function()
			{
				tour.push($(this));
				total_steps = tour.length;
			});
		},

		startTour: function() 
		{
			tourInProgress = true;
			settings.onStart.apply();
			step = 0;
			methods.showControls(function()
			{
				methods.nextStep();
			});
		},
	
		nextStep: function ()
		{
			if (step > 0)
				$('.prevstep').removeClass("disabled").removeAttr("disabled");
			else
				$('.prevstep').addClass("disabled").attr("disabled", true);
			
			if (step == total_steps - 1)
				$('.nextstep').html("Close");
			else
				$('.nextstep').html("Next &raquo;");

			if (step >= total_steps)
			{
				//if last step then end tour
				methods.endTour();
				return false;
			}

			step++;
			methods.showTooltip();
		},

		prevStep: function() 
		{
			if (step > 2)
				$('.prevstep').removeClass("disabled").removeAttr("disabled");
			else
				$('.prevstep').addClass("disabled").attr("disabled", true);

			if (step == total_steps)
				$('.nextstep').html("Next &raquo;");

			if (step <= 1)
				return false;
			step --;

			methods.showTooltip();
		},
	
		endTour: function()
		{
			methods.removeTooltip();
			methods.hideControls();
			$(".tooltip").find("nav").remove();
			$(".tooltip").show();
			settings.onEnd.apply();
			tourInProgress = false;
		},
	
		showTooltip: function()
		{
			var $o, $c, position, props;
			var $elem = tour[step - 1];
			var $tooltip = $elem.nextAll(".tooltip").first();

			$(".tooltip")
			    .css("z-index", 0)
			    .each(function()
			    {
			    	var p = $(this).data("position");
			    	if (p)
			    	{
			    		$(this).removeData("position");
						$(this).animate(
							p,
							500, 
							'easeOutExpo'
						);
			    	}
			    })
			    .find("nav")
			    .remove();

			// We want to move other post-it's out of the way
			var collisions = $elem.collision(
				".tooltip", 
				{ 
					relative: "collider", 
					obstacleData: "odata", 
					colliderData: "cdata", 
					directionData: "ddata", 
					as: "<div/>" 
				}
			);

			for (var i = 0; i < collisions.length; i++)
			{
				$c = $(collisions[i]);
				$o = $c.data("odata");
				// As long as it's not the current tooltip
				if ($o[0] !== $tooltip[0])
				{
					// Remember where it was
					position = $o.position();
					$o.data("position", position);

					// Move it away according to its quadrant
					switch ($c.data("ddata"))
					{
						case "N":  { props = { top: position.top - $c.outerHeight() }; break; }
						case "NE": { props = { top: position.top - $c.outerHeight(), left: position.left + $c.outerWidth() }; break; }
						case "E":  { props = { left: position.left + $c.outerWidth() }; break; }
						case "SE": { props = { top: position.top + $c.outerHeight(), left: position.left + $c.outerWidth() }; break; }
						case "S":  { props = { top: position.top + $c.outerHeight() }; break; }
						case "SW": { props = { top: position.top + $c.outerHeight(), left: position.left - $c.outerWidth() }; break; }
						case "W":  { props = { left: position.left - $c.outerWidth() }; break; }
						case "NW": { props = { top: position.top - $c.outerHeight(), left: position.left - $c.outerWidth() }; break; }
					}

					$o.animate(
						props,
						500, 
						'easeOutExpo'
					);
				}
			}

			if ($tooltip.find("nav").length == 0)
				$('<nav>' + (step > 1 ? '<span class="prevstep">&laquo; Previous</span>' : '') + (step <= total_steps ? '<span class="nextstep">' + (step == total_steps ? 'Close' : 'Next &raquo;') + '</span>' : '') + '</nav>').appendTo($tooltip);

			$tooltip.css("z-index", 1);
			
			var e_w	= $elem.outerWidth();
			var e_h	= $elem.outerHeight();
			var e_l	= $elem.offset().left;
			var e_t	= $elem.offset().top;

			var properties = $tooltip.offset();
			
			// If the element is not in the viewport, we scroll to it before displaying the tooltip
			var w_t	= $(window).scrollTop();
			var w_b = $(window).scrollTop() + $(window).height();

			// get the boundaries of the element + tooltip
			var b_t = parseFloat(properties.top,10);
			
			if(e_t < b_t)
				b_t = e_t;
			
			var b_b = parseFloat(properties.top,10) + $tooltip.height();
			if((e_t + e_h) > b_b)
				b_b = e_t + e_h;
				
			if((b_t < w_t || b_t > w_b) || (b_b < w_t || b_b > w_b))
			{
				$('html, body')
					.stop()
					.animate({scrollTop: b_t - 150}, 500, 'easeInOutExpo', function()
					{
						// Call any onStep handlers
						settings.onStep.apply(this, [$elem]);
					});
			}
			else
			{
				// Call any onStep handlers
				settings.onStep.apply(this, [$elem]);
			}
		},
	
		removeTooltip: function()
		{
			$('#tour_tooltip').remove();
		},
	
		showControls: function(onFinish)
		{
			var $tourcontrols;
			if (onFinish == null)
				onFinish = $.noop;

			if ($("#tourcontrols").length == 0)
			{
				$tourcontrols = $(
					'<div id="tourcontrols" class="tourcontrols" style="display: none;">' +
					    '<nav>' +
					    	'<button class="prevstep">&laquo; Previous</button>' +
					    	'<button class="nextstep">Next &raquo;</button>' +
					    '</nav>' +
				    	'<h1>' + settings.title + '</h1>' +
					    '<span class="close endtour"></span>' +
					'</div>');
				
				$('body').prepend($tourcontrols);
				$tourcontrols.slideDown("fast", function()
				{
					onFinish();
				});
			}
			else
			{
				$("#tourcontrols").show();
				onFinish();
			}
		},
	
		hideControls: function()
		{
			$('#tourcontrols').slideUp("fast");
		},
	};

	$('.starttour').live('click', function() { methods.startTour.apply(); return false; });
	$('.endtour').live('click',   function() { methods.endTour.apply();   return false; });
	$('.nextstep').live('click',  function() { methods.nextStep.apply();  return false; });
	$('.prevstep').live('click',  function() { methods.prevStep.apply();  return false; });

    $(document).keyup(function(evt)
    {
    	if (tourInProgress)
    	{
	        switch (evt.keyCode)
	        {
				// right
				case (39):
				{
					methods.nextStep.apply();
					break;
				}

				// left
				case (37):
				{
					methods.prevStep.apply();
					break;
				}

				// esc
	        	case (27):
	        	{
	        		methods.endTour.apply();
	        		break;
	        	}
	        }
	    }
    });

    $.fn.featureTour = function(method)
    {
        if (methods[method])
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof(method) === 'object' || !method)
            return methods.init.apply(this, arguments);
        else
            $.error('Method ' +  method + ' does not exist on jQuery.featureTour');
    }

})(jQuery);

(function($)
{
    var $ot, $or, $ob, $ol, $tl, $tr, $br, $bl, $frame;

	// To determine the size of the screen on mobile devices
	if (!window.devicePixelRatio) {
	    window.devicePixelRatio = 1;
	}

    var methods = {
    
        getDocWidth: function()
        {
            return $("body").outerWidth();
        },

        getDocHeight: function()
        {
            return $(document).height();
        },

        getWinWidth: function()
        {
            return $("body").outerWidth();
        },

        getWinHeight: function()
        {
            return $(window).height();
        },

        insertElements: function()
        {
        	if ($(".frame").length == 0)
        	{
			    var docHeight = methods.getDocHeight();
			    var docWidth  = methods.getDocWidth();
			    var scrollTop = $(window).scrollTop();
			    var winHeight = methods.getWinHeight();

				$ot = $("<div class='overlay overlay-top' style='height: 0px; top: 0px;'></div>").appendTo("body");
				$or = $("<div class='overlay overlay-right' style='height: " + (scrollTop + winHeight) + "px; top: 0px;'></div>").appendTo("body");
				$ob = $("<div class='overlay overlay-bottom' style='height: 0px; bottom: " + (0 - scrollTop) + "px;'></div>").appendTo("body");
				$ol = $("<div class='overlay overlay-left' style='height: " + (scrollTop + winHeight) + "px; top: 0px;'></div>").appendTo("body");
				$tl = $("<div class='corner corner-top-left'></div>").appendTo("body");
				$tr = $("<div class='corner corner-top-right'></div>").appendTo("body");
				$br = $("<div class='corner corner-bottom-right'></div>").appendTo("body");
				$bl = $("<div class='corner corner-bottom-left'></div>").appendTo("body");
				$frame = $("<div class='frame'></div>").appendTo("body");

				// Make sure that all styles are computed before returning, to allow any subsequent CSS transitions to run
				// Thanks to http://timtaubert.de/blog/2012/09/css-transitions-for-dynamically-created-dom-elements/
				window.getComputedStyle($ot[0]).height;
        	}
        },

        highlight: function()
        {
            return this.each(function()
            {
                var $this = $(this);
				var padding = 20;

                var height = $this.outerHeight() + padding;
                var width  = $this.outerWidth() + padding;
                var offset = $this.offset();
                offset.top -= padding / 2;
                offset.left -= padding / 2;
                var scrollTop = $(window).scrollTop();

                var winHeight = methods.getWinHeight();
                var winWidth = methods.getWinWidth();

                docHeight = methods.getDocHeight();
                docWidth  = methods.getDocWidth();

                methods.insertElements();

                $ot.css("height", offset.top + "px");
                
                $or.css({
                	height: height + "px",
                	width: (docWidth - (width + offset.left)) + "px",
                	top: offset.top + "px"
                });

                $ob.css({
                	height: (docHeight - (offset.top + height)) + "px",
                	bottom: -(docHeight - winHeight) + "px"
                });
                
                $ol.css({
                	height: height + "px",
                	width: offset.left + "px",
                	top: offset.top + "px"
                });

                $tl.css({
                	left: offset.left + "px",
                	top: offset.top + "px"
                });

                $tr.css({
                	left: (offset.left + width - 20) + "px",
                	top: offset.top + "px"
                });

                $br.css({
                	left: (offset.left + width - 20) + "px",
                	top: (offset.top + height - 20) + "px"
                });

                $bl.css({
                	left: offset.left + "px",
                	top: (offset.top + height - 20) + "px"
                });

                $frame.css({
                	height: height + "px",
                	width: width + "px",
                	left: offset.left + "px",
                	top: offset.top + "px"
                });

                // If the frame wasn't visible, make sure it shows up only after the animation has finished
                if ($frame.css("display") == "none")
                {
	                setTimeout(
	                    function()
	                    {
	                        $(".corner").show();
	                        $frame.show();

	                        // Call any highlight events tied to this element
	                        $this.trigger("highlight");
	                    },
	                    100
	                );
	            }
	            else
	            {
                    // Call any highlight events tied to this element
                    $this.trigger("highlight");
	            }
            });
        },

        hide: function(immediately)
        {
            if ($frame && $frame.css("display") != "none")
            {
                docHeight = methods.getDocHeight();
                docWidth  = methods.getDocWidth();

                if (immediately)
                {
                    $(".overlay").hide();
                    $(".overlay,.corner,.frame").remove();
                }

                $(".corner").hide();
                $frame.hide();

                $ot.css("height", "0px");
                $or.css({ height: docHeight + "px", width: "0px", top: "0px" });
                $ob.css("height", "0px");
                $ol.css({ height: docHeight + "px", width: "0px", top: "0px" });

                setTimeout(function() {
                	$(".overlay,.corner,.frame").remove();
	                },
	                100
                );
            }
        }
    };

    $(".overlay").live("click", function()
    {
        methods["hide"].apply();
    });

    $(document).keyup(function(evt)
    {
        if (evt.keyCode == 27)
            methods["hide"].apply();
    });

    $(window).resize(function()
    {
        methods["hide"].apply(this, [true]);
    });

    $.fn.highlight = function(method)
    {
        if (methods[method])
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof(method) === 'object' || !method)
            return methods.highlight.apply(this, arguments);
        else
            $.error('Method ' +  method + ' does not exist on jQuery.highlight');
    }
    
})(jQuery);