$(function()
{
	var $h1 = $("h1");
	var title = $h1.html() + (typeof($h1.attr('data-subtitle')) != 'undefined' ? ' - <span class="subtitle">' + $h1.attr('data-subtitle') + '</span>': '');
		
	// Initiate a feature tour of all the stops
	$(".stop:visible").featureTour({

		title: title,
		onStart: function() 
		{
			$(".starttour").each(function()
			{
				var $button = $(this);

				$button
					.data("html", $button.html())
					.html("End Tour")
					.removeClass("starttour")
					.addClass("endtour");
			});
		},

		onStep: function($el)
		{
			$el.highlight();
		},

		onEnd: function()
		{
			$("body").highlight("hide");
			$(".endtour").each(function()
			{
				var $button = $(this);

				$button
					.html($button.data("html"))
					.removeClass("endtour")
					.addClass("starttour");
			});
		}
	});

	logDimensions = function()
	{
		var pos = $(this).position();
		var left = 100 * pos.left / $(this).parent().outerWidth();
		var top = 100 * pos.top / $(this).parent().outerHeight();
		var width = 100 * $(this).outerWidth() / $(this).parent().outerWidth();
		var height = 100 * $(this).outerHeight() / $(this).parent().outerHeight();
		var allClasses = $(this).attr("class").split(" ");
		var classes = Array();

		for (var c = 0; c < allClasses.length; c ++)
		{
			if (!allClasses[c].match(/^ui/))
				classes.push(allClasses[c]);
		}
			
		console.log(":left => '" + left + "%', :top => '" + top + "%'");
		console.log("#" + $(this).attr("id") + "." + classes.join(".") + "{ :style => 'left: " + left + "%; top: " + top + "%; width: " + width + "%; height: " + height + "%;' }");
	};

	var showMovables;

	$(document).keyup(function(evt)
	{
		if (evt.keyCode == 83)
		{
			if (showMovables)
			{
				$(".tooltip,.stop,.tour-element").css("border", "none");
				showMovables = false;
			}
			else
			{
				$(".tooltip,.stop,.tour-element").css("border", "1px dashed red").resizable({ stop: logDimensions }).draggable({ stop: logDimensions });
				showMovables = true;
			}
		}
	});

	setTimeout(function()
	{
		$(".starttour").addClass("visible-tip");
		setTimeout(function()
		{
			$(".visible-tip").removeClass("visible-tip");
		},
		5000);
	},
	2000);
});

