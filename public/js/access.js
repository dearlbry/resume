$(function()
{
	var show = false;

	if(!!document.createElement('video').canPlayType)
    {
		var vidTest = document.createElement("video");
		if (vidTest.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"') == "probably" || vidTest.canPlayType('video/ogg') == "maybe")
			show = true;
	}
	
	if (show)
	{
		$("#stop_video").bind("highlight", function()
		{
			$("#access_example").show().get(0).play();
		});
	}
});