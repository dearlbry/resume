# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    David Bryant -- Developer -- @dearlbry

# THANKS

    HTML5 Boilerplate - https://github.com/h5bp/html5-boilerplate

# TECHNOLOGY COLOPHON

    Ruby, Sinatra
    HTML5, CSS3
    jQuery, Modernizr
