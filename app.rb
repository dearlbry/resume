require 'sinatra'
require 'sinatra/content_for'
require 'haml'
require_relative 'helpers'

get '/' do
  haml :index
end

get '/resume' do
  redirect '/cv'
end

get %r{/([\w-]+)} do
  if File.exists?(File.expand_path(File.join(File.dirname(__FILE__), "views", params[:captures].first + ".haml")))
    response.headers['Cache-Control'] = 'public, max-age=604800'
    haml params[:captures].first.to_sym 
  else
    status 404
  end
end

not_found do
  haml '404'.to_sym
end