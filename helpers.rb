require 'pp'

def tooltip(options, &block)

	@styles ||= ''
	@tooltip ||= 0

	@tooltip += 1
  html = capture_haml(&block)
  tilt = (rand(4) - 2).to_s
  
  @styles += '#tooltip_' + @tooltip.to_s + ' { ' + options.map { |k,v| ' ' + k.to_s + ': ' + v.to_s + '; ' unless k == :class }.join() + ' -webkit-transform: rotate(' + tilt + 'deg); -moz-transform: rotate(' + tilt + 'deg); -o-transform: rotate(' + tilt + 'deg); transform: rotate(' + tilt + 'deg); }' + "\n"
  '<div id="tooltip_' + @tooltip.to_s + '" class="tooltip' + (options[:class].nil? ? '' : ' ' + options[:class]) + '">' + html + '</div>'

end

def render_title

	title = yield_content(:title)
	title += '|| ' unless title.empty?
	title + 'David Bryant'

end

def render_tooltip_styles

	return "<style type=\"text/css\">
			
			@media only screen and (min-width: 480px) {
				#{@styles}	
			}

		</style>" unless @styles.nil?

end

def render_menu(items)

	li = []

	items.each do |k, v|
		if (request.env['REQUEST_PATH'] == v)
			li.push("<li><span class=\"selected\">#{k}</span></li>")
		else
			li.push("<li><a title=\"#{k}\" alt=\"#{k}\" href=\"#{v}\">#{k}</a></li>")
		end
	end

	'<ul>' + li.join("\n") + '</ul>'

end